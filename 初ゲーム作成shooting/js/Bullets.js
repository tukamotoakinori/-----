var Bullets = enchant.Class.create(Sprite,{
  initialize: function(){
    enchant.Sprite.call(this, 10, 10);
    this.backgroundColor = "yellow"

    this.x = -100;
    this.onenterframe = this.move;
  },

  move: function(){
    this.y -= 15;
    if(this.y < 0){
      this.x = -100;
    }
  }
});
