var Player = enchant.Class.create(Sprite,{
  initialize: function(){
    enchant.Sprite.call(this, 60, 60);
    this.image = GAME.assets["./img/player.png"];
    this.x = (WINDOW_WIDTH - 60) / 2;
    this.y = (WINDOW_HEIGHT - 80);


    this.onenterframe = function(){
      this.move();
    }
  },

  move: function(){
    if(GAME.input.left){
      this.x -=15;
      if(this.x < 0){
        this.x = 0;
      }
    }
    if(GAME.input.right){
      this.x +=15;
      if(this.x > WINDOW_WIDTH - 60){
        this.x = WINDOW_WIDTH - 60;
      }
    }
  }




});
