var Enemy = enchant.Class.create(Sprite,{
  initialize: function(num){
    enchant.Sprite.call(this, 60, 60);
    this.image = GAME.assets["./img/enemy.png"];
    this.speed = 5 + Math.random() * 10;

    this.x = Math.random() * 480;
    this.y = -num * 100;
    this.hp = 5;
    this.onenterframe = this.move;
  },


  move: function(){
    this.rotate(8);
    this.y += this.speed;
    if(this.y > WINDOW_HEIGHT){
      this.init();
    }
  },

  init: function(){
    this.speed = 5 + Math.random() * 10;
    this.x = Math.random() * 480;
    this.y = -60;
    this.hp = 5;
  },

  damage: function(){
    this.hp--;
    if(this.hp == 0){
      this.init();
      GAME.score += 100;
      GAME.scoreLabel.text = "SCORE:" + GAME.score;
    }
  }


});
