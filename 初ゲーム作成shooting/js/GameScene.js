var GameScene = enchant.Class.create(Scene,{
  initialize: function(){
    enchant.Scene.call(this, WINDOW_WIDTH, WINDOW_HEIGHT);
    this.backgroundColor = "black";

    this.player = new Player()
    //画面に貼り付け
    this.addChild(this.player);

    this.bulletNum = 0;
    this.bulletSpan = 0;
    this.bullets = new Array(15);
    for(var i = 0; i < 15; i++){
      this.bullets[i] = new Bullets();
      this.addChild(this.bullets[i]);
    }



    this.enemys = new Array(6);
    for(var i = 0; i < 6; i++){
      this.enemys[i] = new Enemy(i);
      this.addChild(this.enemys[i]);
    }


    //1秒間にFPS回(32回)読み込む
    this.onenterframe = function(){
      this.bulletSpan--;
      this.shot();
      this.contact();
    }

    GAME.score = 0;
    GAME.scoreLabel = new Label();
    GAME.scoreLabel.width = WINDOW_WIDTH;
    GAME.scoreLabel.text = "SCORE:" + GAME.score;
    GAME.scoreLabel.color = "white";
    GAME.scoreLabel.font = "25px cursive"
    this.addChild(GAME.scoreLabel);

  },

  //玉を撃つ
  shot: function(){
    if(GAME.input.a){
      if(this.bulletSpan < 0){
        this.bullets[this.bulletNum].x = this.player.x + ((60 - 10) / 2);
        this.bullets[this.bulletNum].y = this.player.y;
        this.bulletNum++;
        if(this.bulletNum == 15){
          this.bulletNum = 0;
        }
        this.bulletSpan = 5;
      }
    }
  },


    //敵と自分の当たり判定
    contact: function(){
      for(var i = 0; i < 6; i++){
        if(this.enemys[i].within(this.player, 50)){
          var gameOver = new Scene(WINDOW_WIDTH, WINDOW_HEIGHT);
          gameOver.back = new Sprite(WINDOW_WIDTH, WINDOW_HEIGHT);
          gameOver.back.backgroundColor = "black";
          gameOver.back.opacity = 0.5;
          gameOver.addChild(gameOver.back);
          GAME.pushScene(gameOver);
        }
      }

      //玉と敵の当たり判定
        for(var i = 0; i < 6; i++){
          for(var j = 0; j < 15; j++){
            if(this.enemys[i].intersect(this.bullets[j])){
              this.bullets[j].x = -100;
              this.enemys[i].damage();
            }
          }
        }
    }



});
